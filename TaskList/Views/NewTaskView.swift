//
//  NewTaskView.swift
//  TaskList
//
//  Created by Nick Beznos on 2/24/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import SwiftUI

struct NewTaskView: View {
    var taskStore: TaskStore
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var text = ""
    @State var taskPriority: Task.Priority = .no
    
    var body: some View {
        Form {
            TextField("Task name", text: $text)
            
            VStack {
            Text("Priority:")
            
            Picker("Priority", selection: $taskPriority.caseIndex) {
                ForEach(Task.Priority.AllCases.Indices) { priorityIndex in
                    Text(
                        Task.Priority.AllCases[priorityIndex].rawValue
                        .capitalized)
                    .tag(priorityIndex)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            }
            Button("Add") {
                let priorityIndex = self.taskStore.getIndex(for: self.taskPriority)
                self.taskStore.prioritizedTasks[priorityIndex].tasks.append(
                    TaskStore.PrioritizedTasks.init(priority: self.taskPriority, tasks: [Task(name: self.text)]))
                self.presentationMode.wrappedValue.dismiss()
            }
            .disabled(text.isEmpty)
            
        }
    }
}

struct NewTaskView_Previews: PreviewProvider {
    static var previews: some View {
        NewTaskView(taskStore: TaskStore())
    }
}
