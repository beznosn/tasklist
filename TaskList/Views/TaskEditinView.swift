//
//  TaskEditinView.swift
//  TaskList
//
//  Created by Nick Beznos on 2/25/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import SwiftUI

struct TaskEditinView: View {
    @Binding var task: Task
    var body: some View {
        Form {
            TextField("Name", text: $task.name)
            Toggle("Completed", isOn: $task.completed)
        }
    }
}

struct TaskEditinView_Previews: PreviewProvider {
    static var previews: some View {
        TaskEditinView(
            task: .constant(Task(name: "To DO")))
    }
}
