//
//  TaskStore.PrioritizedTasks.swift
//  TaskList
//
//  Created by Nick Beznos on 2/25/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

extension TaskStore {
    struct PrioritizedTasks {
        let priority: Task.Priority
        var tasks: [Task]
    }
}

extension TaskStore.PrioritizedTasks: Identifiable {
    var id: Task.Priority { priority }
}
