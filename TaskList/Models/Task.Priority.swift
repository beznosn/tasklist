//
//  Task.Priority.swift
//  TaskList
//
//  Created by Nick Beznos on 2/25/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

extension Task {
    
    enum Priority: String, CaseIterable {
        case no, low, medium, high
    }
}
