//
//  Task.swift
//  TaskList
//
//  Created by Nick Beznos on 2/24/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

struct Task: Identifiable {
    var name: String
    var completed = false
    
    let id = UUID()
}
