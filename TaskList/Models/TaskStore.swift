//
//  TaskStore.swift
//  TaskList
//
//  Created by Nick Beznos on 2/24/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation
import Combine

class TaskStore: ObservableObject {
    @Published var prioritizedTasks = [PrioritizedTasks(priority: .high, names: ["Task 1", "Task 2", "Task 3",]), PrioritizedTasks(priority: .medium, names: ["Task 4", "Task 5",]), PrioritizedTasks(priority: .low, names: ["Task 6", "Task 7",]), PrioritizedTasks(priority: .no, names: ["Task 8", "Task 9",])]
       
    func getIndex(for priority: Task.Priority) -> Int {
        prioritizedTasks.firstIndex { $0.priority == priority }!
    }
    
}

private extension TaskStore.PrioritizedTasks {
    init(priority: Task.Priority, names: [String]) {
        self.init(priority: priority, tasks: names.map { Task.init(name: $0) })
    }
}
